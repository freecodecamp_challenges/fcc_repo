import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

plt.style.use('tableau-colorblind10')

# Import data (Make sure to parse dates. Consider setting index column to 'date'.)
df = pd.read_csv('fcc/boilerplate-page-view-time-series-visualizer-main/fcc-forum-pageviews.csv',
                 index_col='date', parse_dates=True)

# Clean data
lowest_values = df['value'].quantile(0.025)
highest_values = df['value'].quantile(0.975)

df = df[(df['value'] <= highest_values) &
        (df['value'] >= lowest_values)]


def draw_line_plot():
    # Create a Figure object
    fig, ax = plt.subplots(figsize=(18, 6))

    # Draw line plot on the axes
    ax.plot(df.index, df['value'], 'orange', alpha=0.85)

    # Add title and labels
    ax.set_title('Daily freeCodeCamp Forum Page Views 5/2016-12/2019')
    ax.set_xlabel('Date')
    ax.set_ylabel('Page Views')

    # Save image and return fig (don't change this part)
    fig.savefig('line_plot.png')
    return fig


def draw_bar_plot():
    # Copy and modify data for monthly bar plot
    df_bar = df.groupby([df.index.year, df.index.month]).agg({'value': 'mean'})

    # Draw bar plot
    ax = df_bar.unstack().plot.bar()

    # Add title and labels
    plt.title('Daily freeCodeCamp Forum Page Views 5/2016-12/2019')
    plt.xlabel('Years')
    plt.ylabel('Average Page Views')
    # Add legend
    legend = ('January', 'February', 'March', 'April', 'May', 'June',
              'July', 'August', 'September', 'October', 'November', 'December')
    plt.legend(title='Months', labels=legend)

    # Get the Figure object from the Axes object
    fig = ax.get_figure()

    # Save image and return fig (don't change this part)
    fig.savefig('bar_plot.png')
    return fig


def draw_box_plot():
    # Prepare data for box plots (this part is done!)
    df_box = df.copy()
    df_box.reset_index(inplace=True)
    df_box['year'] = [d.year for d in df_box.date]
    df_box['month'] = [d.strftime('%b') for d in df_box.date]

    # Define subplots (rows, columns, size)
    fig, ax = plt.subplots(1, 2, figsize=(18, 6))

    # Draw box plots (using Seaborn)
    # .boxplot() includes outliers by default, they are represented by dots (or lines) that are separate from the boxes. If data has outliers, extra lines are added, causing tests to fail.
    # To fix this: add showfliers=False
    ax1 = sns.boxplot(data=df_box, x='year', y='value',
                      showfliers=False, fill=False, ax=ax[0])

    # y-axis labels are expected to range from 0 to 200000 in increments of 20000, but data maxes at 160000.
    # To be set manually otherwise tests fails
    # To fix this: call set_ylim and set_yticks directly on the Axes object and set y-axis limit and y-ticks
    ax1.set_ylim([0, 200000])
    ax1.set_yticks(range(0, 200001, 20000))

    # Add labels and title
    ax1.set(
        xlabel='Year',
        ylabel='Page Views',
        title='Year-wise Box Plot (Trend)'
    )

    ax2 = sns.boxplot(data=df_box, x='month', y='value', showfliers=False, order=[
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ], fill=False, ax=ax[1])
    ax2.set(
        xlabel='Month',
        ylabel='Page Views',
        title='Month-wise Box Plot (Seasonality)'
    )

    # Set y-axis limit and y-ticks again
    ax2.set_ylim([0, 200000])
    ax2.set_yticks(range(0, 200001, 20000))

    # Save image and return fig (don't change this part)
    fig.savefig('box_plot.png')
    return fig
