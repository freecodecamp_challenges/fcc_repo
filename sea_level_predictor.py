import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import linregress


def draw_plot():
    # Read data from file
    data = pd.read_csv(
        'fcc/boilerplate-sea-level-predictor-main/epa-sea-level.csv')

    # Prepare data
    x = data['Year']
    y = data['CSIRO Adjusted Sea Level']

    # Create scatter plot
    plt.scatter(x, y, color='xkcd:sky blue')

    # Create first line of best fit
    result = linregress(x, y)
    x_fitline1 = range(x.min(), 2051)
    y_fitline1 = result.intercept + result.slope * x_fitline1
    plt.plot(x_fitline1, y_fitline1, 'teal')

    # Create second line of best fit
    # Prepare data
    recent_data = data[data['Year'] >= 2000]
    w = recent_data['Year']
    z = recent_data['CSIRO Adjusted Sea Level']

    # Plot the line
    result_line2 = linregress(w, z)
    x_fitline2 = range(w.min(), 2051)
    y_fitline2 = result_line2.intercept + result_line2.slope * x_fitline2
    plt.plot(x_fitline2, y_fitline2, 'lime')

    # Add title and labels
    plt.title('Rise in Sea Level')
    plt.xlabel('Year')
    plt.ylabel('Sea Level (inches)')

    # Save plot and return data for testing (DO NOT MODIFY)
    plt.savefig('sea_level_plot.png')
    return plt.gca()
