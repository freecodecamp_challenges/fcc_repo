import pandas as pd


def calculate_demographic_data(print_data=True):
    # Read data from file
    df = pd.read_csv(
        'fcc/boilerplate-demographic-data-analyzer-main/adult.data.csv')

    # How many of each race are represented in this dataset? This should be a Pandas series with race names as the index labels.
    race_count = df['race'].value_counts()

    # What is the average age of men?
    average_age_men = round(df[df['sex'] == 'Male'].age.mean(), 1)

    # What is the percentage of people who have a Bachelor's degree?
    education_ratio = df.education.value_counts(normalize=True)
    percentage_bachelors = round((education_ratio['Bachelors'])*100, 1)

    # What percentage of people with advanced education (`Bachelors`, `Masters`, or `Doctorate`) make more than 50K?
    # What percentage of people without advanced education make more than 50K?

    # Education level masks
    higher_education = df[(df['education'].isin(
        ['Bachelors', 'Masters', 'Doctorate']))]
    lower_education = df[(~df['education'].isin(
        ['Bachelors', 'Masters', 'Doctorate']))]

    # Filters
    filtered_education = df[(df['education'].isin(
        ['Bachelors', 'Masters', 'Doctorate'])) & (df['salary'] == '>50K')]
    filtered_no_education = df[(~df['education'].isin(
        ['Bachelors', 'Masters', 'Doctorate'])) & (df['salary'] == '>50K')]

    # percentage with salary >50K
    higher_education_rich = round(
        (len(filtered_education) / len(higher_education)) * 100, 1)
    lower_education_rich = round(
        (len(filtered_no_education) / len(lower_education)) * 100, 1)

    # What is the minimum number of hours a person works per week (hours-per-week feature)?
    min_work_hours = df['hours-per-week'].min()

    # What percentage of the people who work the minimum number of hours per week have a salary of >50K?
    num_min_workers = df[df['hours-per-week'] == df['hours-per-week'].min()]
    num_min_rich_workers = len(
        num_min_workers[num_min_workers['salary'] == '>50K'])

    rich_percentage = round((num_min_rich_workers/len(num_min_workers))*100, 1)

    # What country has the highest percentage of people that earn >50K?
    # Compute a frequency table of salary types by country
    df1 = (pd.crosstab(df['native-country'], df['salary'], normalize='index')
           .mul(100)
           .reset_index()
           .rename_axis(None, axis=1))

    # Sort the values so the highest percentage of '>50K' is indexed on the first row
    df1 = df1.sort_values(['>50K'], ascending=False).reset_index()

    # Retrieve the values from the first row of the sorted dataframe
    highest_earning_country = df1.at[0, 'native-country']
    highest_earning_country_percentage = round(df1.at[0, '>50K'], 1)

    # Identify the most popular occupation for those who earn >50K in India.
    df_india_rich = df[(df['salary'] == '>50K') &
                       (df['native-country'] == 'India')]

    # Find the most popular occupation
    top_IN_occupation = df_india_rich['occupation'].value_counts().idxmax()

    # DO NOT MODIFY BELOW THIS LINE

    if print_data:
        print('Number of each race:\n', race_count)
        print('Average age of men:', average_age_men)
        print(f'Percentage with Bachelors degrees: {percentage_bachelors}%')
        print(
            f'Percentage with higher education that earn >50K: {higher_education_rich}%')
        print(
            f'Percentage without higher education that earn >50K: {lower_education_rich}%')
        print(f'Min work time: {min_work_hours} hours/week')
        print(
            f'Percentage of rich among those who work fewest hours: {rich_percentage}%')
        print('Country with highest percentage of rich:', highest_earning_country)
        print(
            f'Highest percentage of rich people in country: {highest_earning_country_percentage}%')
        print('Top occupations in India:', top_IN_occupation)

    return {
        'race_count': race_count,
        'average_age_men': average_age_men,
        'percentage_bachelors': percentage_bachelors,
        'higher_education_rich': higher_education_rich,
        'lower_education_rich': lower_education_rich,
        'min_work_hours': min_work_hours,
        'rich_percentage': rich_percentage,
        'highest_earning_country': highest_earning_country,
        'highest_earning_country_percentage':
        highest_earning_country_percentage,
        'top_IN_occupation': top_IN_occupation
    }
