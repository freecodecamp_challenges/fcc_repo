import itertools

def arithmetic_arranger(problems, *args):
    list = []
    outputs = []

    if len(problems) > 5:
        arranged_problems = "Error: Too many problems."
        return arranged_problems

    for problem in problems:

        arranged_problems = ""
        list = problem.split(" ")

        if len(list[0]) > 4 or len(list[2]) > 4:
            arranged_problems = "Error: Numbers cannot be more than four digits."
            return arranged_problems

        if list[0].isdigit() == False or list[2].isdigit() == False:
            arranged_problems = "Error: Numbers must only contain digits."
            return arranged_problems

        if list[1] != "+" and list[1] != "-":
            arranged_problems = "Error: Operator must be '+' or '-'."
            return arranged_problems

        dash_len = len(max(list, key=len)) + 2
        dash_line = "-" * dash_len
        line1 = list[0].rjust(dash_len)
        line2 = list[1].ljust(0)+list[2].rjust(dash_len-1)

        if args:
            # do not use eval
            result = eval(f"{list[0]}{list[1]}{list[2]}")
            result_line = str(result).rjust(dash_len)
            outputs.append(
                [str(line1), str(line2), str(dash_line), str(result_line)])
        else:
            outputs.append([str(line1), str(line2), str(dash_line)])

        arranged_problems = "\n".join("    ".join(
            p if p else "" for p in parts) for parts in itertools.zip_longest(*outputs))

    return arranged_problems