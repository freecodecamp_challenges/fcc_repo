import numpy as np


def calculate(list):
    if len(list) == 9:
        list = np.array(list).reshape(3, 3)
        flat = list.flatten()

        mean_ax0 = list.mean(axis=0)
        mean_ax1 = list.mean(axis=1)
        mean_flat = flat.mean()

        var_ax0 = np.var(list, axis=0)
        var_ax1 = np.var(list, axis=1)
        var_flat = np.var(flat)

        std_ax0 = np.std(list, axis=0)
        std_ax1 = np.std(list, axis=1)
        std_flat = np.std(flat)

        min_ax0 = np.min(list, axis=0)
        min_ax1 = np.min(list, axis=1)
        min_flat = np.min(flat)

        max_ax0 = np.max(list, axis=0)
        max_ax1 = np.max(list, axis=1)
        max_flat = np.max(flat)

        sum_ax0 = np.sum(list, axis=0)
        sum_ax1 = np.sum(list, axis=1)
        sum_flat = np.sum(flat)

        calculations = {'mean': [mean_ax0.tolist(), mean_ax1.tolist(), mean_flat],
                        'variance': [var_ax0.tolist(), var_ax1.tolist(), var_flat],
                        'standard deviation': [std_ax0.tolist(), std_ax1.tolist(), std_flat],
                        'max': [max_ax0.tolist(), max_ax1.tolist(), max_flat],
                        'min': [min_ax0.tolist(), min_ax1.tolist(), min_flat],
                        'sum': [sum_ax0.tolist(), sum_ax1.tolist(), sum_flat]
                        }

    else:
        raise ValueError("List must contain nine numbers.")

    # print(calculations)
    return calculations
